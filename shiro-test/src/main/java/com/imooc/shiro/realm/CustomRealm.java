package com.imooc.shiro.realm;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author chenfeng
 * @date 2020/3/15 17:11
 */
public class CustomRealm extends AuthorizingRealm {
    
    public CustomRealm(){
        System.out.println("CustomRealm构造函数");
    }

    private Map<String, String> userMap = new HashMap<String, String>(16);
    
    {
        super.setName("customRealm");
    }

    /**
     * 批准授权
     * @param principals
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        //1.从主体传过来的认证信息中获取用户名
        String userName = (String) principals.getPrimaryPrincipal();
        //从数据库或缓存获取角色数据
        Set<String> roles = getRolesByUserName(userName);
        //从数据库或缓存获取权限数据
        Set<String> permissions = getPermissionsByUserName(userName);
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        authorizationInfo.setRoles(roles);
        authorizationInfo.setStringPermissions(permissions);
        return authorizationInfo;
    }

    /**
     * 身份验证
     * @param token
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        //1.从主体传过来的认证信息中获取用户名
        String userName = (String) token.getPrincipal();
        //2.通过用户名到数据库中获取凭证
        String password = getPasswordByUserName(userName);
        if(password == null){
            return null;
        }
        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo("Mark",password,"customRealm");
        //设置盐值
        ByteSource salt = ByteSource.Util.bytes("Mark");
        authenticationInfo.setCredentialsSalt(salt);
        return authenticationInfo;
    }

    /**
     * 模拟从数据库获取角色数据
     * @param userName
     * @return 权限集合
     */
    private Set<String> getRolesByUserName(String userName) {
        Set<String> sets = new HashSet<String>();
        sets.add("admin");
        sets.add("user");
        sets.add("hr");
        return sets;
    }
    
    /**
     * 模拟数据库通过用户名查询
     * @param userName 用户名
     * @return  密码
     */
    private String getPasswordByUserName(String userName) {
        userMap.put("Mark", "d40fdd323f5322ff34a41f026f35cf20");
        userMap.put("chenfeng", "d40fdd323f5322ff34a41f026f35cf20");
        return userMap.get(userName);
    }

    /**
     * 模拟获取权限数据
     * @param userName
     * @return 权限集合
     */
    private Set<String> getPermissionsByUserName(String userName) {
        Set<String> sets = new HashSet<String>();
        sets.add("user:delete");
        sets.add("user:add");
        return sets;
    }

    public static void main(String[] args) {
        Md5Hash md5Hash = new Md5Hash("1234567", "Mark");
        System.out.println(md5Hash);
    }
}
