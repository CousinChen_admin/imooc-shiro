package com.imooc.test;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.SimpleAccountRealm;
import org.apache.shiro.subject.Subject;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author chenfeng
 * @date 2020/3/15 14:22
 */
public class AuthenticationTest {

    Logger logger = LoggerFactory.getLogger(AuthenticationTest.class);

    SimpleAccountRealm simpleAccountRealm = new SimpleAccountRealm();
    
    @Before
    public void addUser(){
        simpleAccountRealm.addAccount("Mark", "123456","admin", "user");
    }
    
    @Test
    public void testAuthentication(){
        
        //1.构建SecurityManager环境
        DefaultSecurityManager defaultSecurityManager = new DefaultSecurityManager();
        defaultSecurityManager.setRealm(simpleAccountRealm);
        
        //2.主体提交认证请求
        SecurityUtils.setSecurityManager(defaultSecurityManager);
        Subject subject = SecurityUtils.getSubject();
        
        UsernamePasswordToken token = new UsernamePasswordToken("Mark", "123456");
        try {
            //登录
            subject.login(token);
        } catch (UnknownAccountException e) {
            e.printStackTrace();
            System.out.println("未知账号异常");
        } catch (IncorrectCredentialsException e) {
            e.printStackTrace();
            System.out.println("凭证错误异常");
        }
        System.out.println("验证isAuthenticated:" + subject.isAuthenticated());
        //退出
//        subject.logout();
        System.out.println("验证isAuthenticated:" + subject.isAuthenticated());
        //角色验证
        subject.checkRoles("admin", "user");
    }
    
    
    
}
