package com.imooc.test;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.jdbc.JdbcRealm;
import org.apache.shiro.subject.Subject;
import org.junit.Test;

/**
 * @author chenfeng
 * @date 2020/3/15 15:41
 */
public class JdbcRealmTest {

    DruidDataSource dataSource = new DruidDataSource();
    
    {
        dataSource.setUrl("jdbc:mysql://localhost:3306/test");
        dataSource.setUsername("root");
        dataSource.setPassword("mysql");
    }

    @Test
    public void testAuthentication(){
        JdbcRealm jdbcRealm = new JdbcRealm();
        jdbcRealm.setDataSource(dataSource);
        jdbcRealm.setAuthenticationQuery("select password from sys_user where user_name = ?");
        jdbcRealm.setUserRolesQuery("select p.role_name from sys_user_roles r\n" +
                "left JOIN sys_user u on r.user_id = u.id\n" +
                "LEFT JOIN sys_roles_permission p on p.id = r.role_id\n" +
                "where u.user_name = ?");
        //1.构建SecurityManager环境
        DefaultSecurityManager defaultSecurityManager = new DefaultSecurityManager();
        defaultSecurityManager.setRealm(jdbcRealm);
        //2.主体提交认证请求
        SecurityUtils.setSecurityManager(defaultSecurityManager);
        Subject subject = SecurityUtils.getSubject();

        UsernamePasswordToken token = new UsernamePasswordToken("Mark", "123456");
        try {
            //登录
            subject.login(token);
        } catch (UnknownAccountException e) {
            e.printStackTrace();
            System.out.println("未知账号异常");
        } catch (IncorrectCredentialsException e) {
            e.printStackTrace();
            System.out.println("凭证错误异常");
        }
        System.out.println("验证isAuthenticated:" + subject.isAuthenticated());
        subject.checkRoles("系统管理员","HR");
    }
}
