package com.imooc.test;

import com.imooc.shiro.realm.CustomRealm;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.subject.Subject;
import org.junit.Test;

/**
 * @author chenfeng
 * @date 2020/3/15 17:30
 */
public class CustomRealmTest {
    
    @Test
    public void testAuthentication(){
        CustomRealm customRealm = new CustomRealm();
        
        //1.构建SecurityManager环境
        DefaultSecurityManager defaultSecurityManager = new DefaultSecurityManager();
        defaultSecurityManager.setRealm(customRealm);
        
        HashedCredentialsMatcher matcher = new HashedCredentialsMatcher();
        //设置加密算法名称
        matcher.setHashAlgorithmName("md5");
        //设置加密次数
        matcher.setHashIterations(1);
        customRealm.setCredentialsMatcher(matcher);
        
        //2.主体提交认证请求
        SecurityUtils.setSecurityManager(defaultSecurityManager);
        Subject subject = SecurityUtils.getSubject();

        UsernamePasswordToken token = new UsernamePasswordToken("Mark", "1234567");
        try {
            //登录
            subject.login(token);
        } catch (UnknownAccountException e) {
            System.out.println("账号错误");
        } catch (IncorrectCredentialsException e) {
            System.out.println("密码错误");
        }
        System.out.println("验证结果isAuthenticated:" + subject.isAuthenticated());
        subject.checkRoles("admin");
        subject.checkPermissions("user:add", "user:delete");
    }
}
